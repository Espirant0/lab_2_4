#include <iostream>
#include <chrono>
#include "functions.hpp"


namespace saw {
    
    Timer::Timer() : m_beg(clock_t::now()) {}
    void Timer::reset() {
        m_beg = clock_t::now();
    }
    double Timer::elapsed() const {
        return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
    }


	void ADD(T_List* head, int age)
	{
		T_List* p = new T_List;
		p->age = age;

		p->next = head->next;
		head->next = p;
	}

	void PRINT(T_List* head)
	{
		T_List* p = head->next;
		while (p != nullptr)
		{
			std::cout << p->age << std::endl;
			p = p->next;
		}
	}

	void DELETE(T_List* head, int& k)
	{
		T_List* tmp;
		T_List* p = head;
		while (p->next != nullptr)
		{
			if (p->next->age == k)
			{
				tmp = p->next;
				p->next = p->next->next;
				delete tmp;
			}
			else
				p = p->next;
		}
	}

	void CLEAR(T_List* head)
	{
		T_List* tmp;
		T_List* p = head->next;
		while (p != nullptr)
		{
			tmp = p;
			p = p->next;
			delete tmp;
		}
	}


}