#include <iostream>
#include <vector>
#include "functions.hpp"


using namespace saw;

int main() {
	int k = 41;
	std::vector<int> vec;
	T_List* head = new T_List;
	head->next = nullptr;

	for (int i = 0; i < 50; i++)
		vec.push_back(rand() % 50);
	
	for (int i = 0; i < 50; i++)
		ADD(head, rand()%50);
	std::cout << "=====VECTOR=====" << std::endl;
	
	for (int i = 0; i < 50; i++)
		std::cout << vec[i] << std::endl;
	std::cout << "=====LIST=====" << std::endl;
	PRINT(head);
	std::cout << "=======================" << std::endl;
	
	Timer timer_list;
	for (int i = 0; i < 1000; i++)
		DELETE(head, k);
	std::cout << "List time: " << timer_list.elapsed() << '\n';
	
	Timer timer_vector;
	for (int i = 0; i < vec.size(); i++)
		if (vec[i] == k)
			vec.erase(vec.begin() + i);
	std::cout << "Vector time: " << timer_vector.elapsed() << '\n';
	
	std::cout << "=====NEW___LIST=====" << std::endl;
	PRINT(head);
	
	std::cout << "=====NEW___VECTOR=====" << std::endl;
	for (int i = 0; i < vec.size(); i++)
		std::cout << vec[i] << std::endl;

	CLEAR(head);
	delete head;
	return 0;
}